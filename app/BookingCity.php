<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingCity extends Model
{
    public static function getCitiesCountByCountryId($countryId){
        return self::where('country_id',$countryId)->count();
    }

    public function addCountryCity($countryId,$cityId,$cityName,$hotelsCount){
        $this->country_id = $countryId;
        $this->city_id = $cityId;
        $this->city_name = $cityName;
        $this->hotels_number = $hotelsCount;
        $this->save();
    }
}
