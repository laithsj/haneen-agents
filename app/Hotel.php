<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    public static function getHotelById($hotelId){
        return self::where('hotel_id',$hotelId)->get();
    }

    public function addNewHotel($hotelId,$defaultLanguage,$address,$hotelTypeId,$url,$cityId,$reviewScore,$deepLinkUrl,$currency,$exactClass,$isClosed,$ranking,$numberOfReviews,$zip,$city,$class,$name,$numberOfRooms,$country,$preferred,$longitude,$latitude){
        $this->hotel_id = $hotelId;
        $this->default_language = $defaultLanguage;
        $this->address = $address;
        $this->hotel_type_id = $hotelTypeId;
        $this->url = $url;
        $this->city_id = $cityId;
        $this->review_score = $reviewScore;
        $this->deep_link_url = $deepLinkUrl;
        $this->currency = $currency;
        $this->exact_class = $exactClass;
        $this->is_closed = $isClosed;
        $this->ranking = $ranking;
        $this->number_of_reviews = $numberOfReviews;
        $this->zip = $zip;
        $this->city = $city;
        $this->class = $class;
        $this->name = $name;
        $this->number_of_rooms = $numberOfRooms;
        $this->country = $country;
        $this->preferred = $preferred;
        $this->longitude = $longitude;
        $this->latitude = $latitude;
        $this->save();

        return $this;
    }

    public static function getHotelsByCountry($iso){
        return self::where('country',$iso)->get();
    }
}
