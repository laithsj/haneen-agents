<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TripHotel extends Model
{

    public function hotel(){
		return $this->belongsTo('App\Hotel','hotel_id','hotel_id');
    }

    public function addTripHotels($tripId,$hotelId){
        $this->trip_id = $tripId;
        $this->hotel_id = $hotelId;
        $this->save();
    }

    public static function removeTripHotels($tripId){
        self::where('trip_id',$tripId)->delete();
    }

    public function addTripHotel($tripId,$hotelId){
        $this->trip_id = $tripId;
        $this->hotel_id = $hotelId;
        $this->save();
    }
}
