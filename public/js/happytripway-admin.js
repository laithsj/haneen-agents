$(document).ready(function() {
    $('#example').DataTable();

    // hide success message
    if($('#successMessage').length !== 0){
        setTimeout(function(){
            $('#successMessage').fadeOut(2000);
        }, 3000);
    }

    // hide fail messages
    if($('#failMessage').length !== 0){
        setTimeout(function(){
            $('#failMessage').fadeOut(2000);
        }, 3000);
    }

    // initate countries input
    if($('#testinput').length !== 0){
        new NiceCountryInput($("#testinput")).init();
    }

    // get countryId
    if($('.niceCountryInputMenuInputHidden').length !== 0){
        // $('.niceCountryInputMenuInputHidden').val('JO');
        $('#testinput').data("selectedcountry");
    }

    // setup select 2 plugin
    if($('.select2').length !== 0){
        $('.select2').select2();
    }

    if($('#tripDate').length !== 0){
        $('#tripDate').daterangepicker();

    }
    // get cities for country
    if($('#tripCountryInternal').length !== 0){
        $("#tripCountryInternal").change(function(){
            $("#spinOverlay").removeClass('hide');
            var countryId = $("#tripCountryInternal").val();
            $.ajaxSetup(
                {
                    url: "/get-country-cities",
                    type: "post",
                    data : {
                        "_token": $('meta[name="csrf-token"]').attr('content'),
                        'countryId' : countryId
                    },
                    success: function(result){
                        $("#countryCitiesInternal").html(result);
                        $("#spinOverlay").addClass('hide');
                    }
                });
            $.ajax();
        });
    }

    // set trip text editor
    if($('#editorCreateTrip').length !== 0){
        ClassicEditor.create( document.querySelector( '#editorCreateTrip' ) ).catch( error => {
                console.error( error );
        });
    }

    // add loader when user create new trip
    if($('#createTrip').length !== 0){
        $("#createTrip").click(function(){
            $('.spinOverlay').removeClass('hide');
        });
    }

    // check price input
    if($('#tripPrice').length !== 0){
        $('#tripPrice').on('keypress input',function (e) {
            var isNum= isNumber(event, this);
            if(isNum){
                if($('#extraPrice').val()){
                    var ExtraPrice = parseFloat($('#extraPrice').val());
                }else{
                    var ExtraPrice = 0;
                }
                $("#totalPrice").val(parseFloat($('#tripPrice').val()) + ExtraPrice);
            }else{
                return isNum;
            }
        });
    }

    if($('#extraPrice').length !== 0){
        $('#extraPrice').on('keypress input',function (e) {
            var isNum= isNumber(event, this);
            if(isNum){
                if($('#tripPrice').val()){
                    var PricePerPerson = parseFloat($('#tripPrice').val());
                }else{
                    var PricePerPerson = 0;
                }
                $("#totalPrice").val(parseFloat($('#extraPrice').val()) + PricePerPerson);
            }else{
                return isNum;
            }
        });
    }

    // get cities for country
    if($('#tripCountryExternalTo').length !== 0){
        $("#tripCountryExternalTo").change(function(){
            $("#spinOverlay").removeClass('hide');
            var countryId = $("#tripCountryExternalTo").val();
            $.ajaxSetup(
                {
                    url: "/get-country-hotels",
                    type: "post",
                    data : {
                        "_token": $('meta[name="csrf-token"]').attr('content'),
                        'countryId' : countryId
                    },
                    success: function(result){
                        // data = $.parseJSON(data);
                        var html = '';
                        $.each(result, function(i, item) {
                            html += '<option value="'+item.hotel_id+'">'+item.name+'</option>';
                        });
                        $("#countryCitiesExternal").html(html);
                        $("#spinOverlay").addClass('hide');
                    }
                });
            $.ajax();
        });
    }

});


function onChangeCallback(ctr){
    console.log("The country was changed: " + ctr);
}

function isNumber(evt, element) {

    var charCode = (evt.which) ? evt.which : event.keyCode

    if (
        (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
        (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
        (charCode < 48 || charCode > 57))
        return false;

    return true;
}
