
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Log In</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="css/main2.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="css/skins/skin-purple-light.min.css ">
        <meta name="csrf-token" content="qPRTP521Gu2cg26qSvNwCEP51IvsiLTFcblwXKS4" />
        <link rel="stylesheet" href="css/log_in.css ">

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
   <body style="background-color:#809fff;">
   <div class="container">
   <div class="row">
   <div class="col-12">
  <div class="card mt-5">
    <div class="card-header pl-5"style="font-weight: 900;
     font-size: 24px;
    color: #809fff;">Thanks</div>
    <div class="card-body">
       Thank you for registration, please wait till your are approved .
  </div> 
  <div class="row">
   <div class="col-12">
    <div style="    text-align: end;
    margin-right: 40px;
    margin-bottom: 6px;
    font-size: 21px">
      <a href="{{action('Auth\LoginController@viewLogin')}}"> Done </a>
    <div>
</div>
</div>
</div>
</div>
</div>

   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
   </body>
</html>



