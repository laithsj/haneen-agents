<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Local Trips</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="css/main.min.css">
    <link rel="stylesheet" href="css/skins/skin-purple-light.min.css ">
    <meta name="csrf-token" content="qPRTP521Gu2cg26qSvNwCEP51IvsiLTFcblwXKS4" />
    <link rel="stylesheet" href="css/localtrip.css ">
    <!-- Google Font -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-purple-light sidebar-mini layout-top-nav">
    <div class="wrapper">
        <!-- Main Header -->
        <header class="main-header">
            <nav class="navbar navbar-static-top"style="background-color:#809fff!important;">
                <div class="container">
                    <div class="navbar-header">
                        <a href="{{action('ProfileController@viewPorfile')}}" class="navbar-brand">
                            <b>happy travels</b>
                        </a>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar-collapse">
                            <i class="fa fa-bars"></i>
                        </button>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li class="active dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-fw fa-plane "></i>
                                    Trip
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li class="active ">
                                        <a href="{{action('TripController@viewTripsList')}}" class="list">
                                            <i class="fa fa-list-alt "></i>
                                            Trips List
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="{{action('TripController@viewCreateInternalTrip')}}"  class="list">
                                            <i class="fa fa-pencil-square "></i>
                                            Create Domestic Trip
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="{{action('TripController@viewTripExternal')}}"  class="list">
                                            <i class="fa fa-pencil-square "></i>
                                            Create International Trip
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="#">
                                <a href="{{action('ProfileController@viewPorfile')}}">
                                    <i class="fa fa-fw fa-user "></i>
                                    Profile
                                </a>
                            </li>
                            <li class="">
                                <a href="{{action('ProfileController@viewEditProfile')}}">
                                    <i class="fa fa-fw fa-edit "></i>
                                    Edit Profile
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                    <!-- Navbar Right Menu -->
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="{{action('Auth\LoginController@logout')}}">
                                    <i class="fa fa-fw fa-power-off"></i> Log Out
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <div class="content-wrapper">
            <div class="container " >
                <!-- Main content -->
<div class="col-md-12 "style="margin-top:2%!important">
    <div class="box box-primary mt-3">
        <div class="box-header with-border">
            <h3 class="box-title"style="font-size: 28px;
    color: #605ca8;">Trip </h3>
        </div>
        <div class="box-body no-padding">
            <div class="mailbox-read-message">
                <div class="box-body">
                    <ul class="timeline">
                        <li class="time-label">
                            <span class="bg-blue">
                                Trip Start On {{date_format(date_create($trip->trip_start_date),'Y-m-d')}}
                            </span>
                        </li>
                        <li>
                            <i class="fa fa-info-circle bg-blue"></i>
                            <div class="timeline-item">
                                <h3 class="timeline-header">Trip Details</h3>
                                <div class="timeline-body">
                                    <dl class="dl-horizontal">
                                        <dt>Trip Countries </dt>
                                        @if($trip->country_id > 0)
                                            <dd>{{$trip->country->name}}</dd>
                                        @else
                                            <dd><i class="fa fa-plane" aria-hidden="true"></i> {{$trip->countryFrom}}</dd>
                                            <dd><i class="fa fa-plane" aria-hidden="true"></i> {{$trip->countryTo}}</dd>
                                        @endif
                                        @if($trip->country_id > 0)
                                        <dt>Trip Cities</dt>
                                            @foreach($trip->cities as $city)
                                                <dd> {{$city->city->name}}</dd>
                                            @endforeach
                                        @else
                                        <dt>Trip Hotels List</dt>
                                            @foreach($trip->hotels as $hotel)
                                                <dd><i class="fa fa-bed" aria-hidden="true"></i> <a href="{{$hotel->hotel->url}}">{{$hotel->hotel->name}}</a></dd>
                                            @endforeach
                                        @endif
                                        <dt>Trip Age Acceptence</dt>
                                            <dd><i class="fa fa-male" aria-hidden="true"></i>
                                                @if($trip->age == 0)
                                                Both Adult & Kids
                                                @elseif($trip->age == 1)
                                                    Only Adult
                                                @else
                                                    Only Kids
                                                @endif
                                            </dd>
                                    </dl>
                                </div>
                            </div>
                        </li>
                        <li>
                            <i class="fa fa-info-circle bg-blue"></i>
                            <div class="timeline-item">
                                <h3 class="timeline-header">Extra Details</h3>
                                <div class="timeline-body">
                                    {!! $trip->extra_details!!}
                                </div>
                            </div>
                        </li>
                        <li>
                            <i class="fa fa-money bg-blue"></i>
                            <div class="timeline-item">
                                <h3 class="timeline-header">Trip Prices</h3>
                                <div class="timeline-body">
                                    <dl class="dl-horizontal">
                                        <dt>Trip Price Per Person</dt>
                                            <dd>{{$trip->currency}} {{$trip->trip_price_per_person}}</dd>
                                        <dt>Trip Extra</dt>
                                            <dd>{{$trip->currency}} {{$trip->trip_extra_price}}</dd>
                                        <dt>Trip Total Price</dt>
                                            <dd>{{$trip->currency}} {{$trip->trip_total_price}}</dd>
                                    </dl>
                                </div>
                            </div>
                        </li>
                        <li class="time-label">
                            <span class="bg-red">
                                Trip Ends On {{date_format(date_create($trip->trip_end_date),'Y-m-d')}}
                            </span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <ul class="mailbox-attachments clearfix">
                <li>
                    <span class="mailbox-attachment-icon"><i class="fa fa-file-zip-o"></i></span>
                    <div class="mailbox-attachment-info">
                        <a href="/trip-files-download?id={{$trip->id}}&enc={{md5($trip->id.Auth::user()->id)}}" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> Trip_{{$trip->id}}.pdf</a>
                        <span class="mailbox-attachment-size">
                            <a href="/trip-files-download?id={{$trip->id}}&enc={{md5($trip->id.Auth::user()->id)}}" class="btn btn-default btn-xs pull-right"><i class="fa fa-cloud-download"></i></a>
                        </span>
                    </div>
                </li>
            </ul>
        </div>
        <div class="box-footer">
            <div class="pull-right" style="margin-right: 3%;">
                <a href="/delete-trip?id={{$trip->id}}&enc={{md5($trip->id.Auth::user()->id)}}" class="btn btn-danger"style="font-size:18px"><i class="fa fa-trash-o"></i> Delete</a>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
