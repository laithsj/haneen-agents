<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_cities', function (Blueprint $table) {
            $table->engine = "MyIsam";
            $table->bigIncrements('id');
            $table->string('city_id',255)->nullable();
            $table->string('city_name',255)->nullable();
            $table->integer('country_id')->unsigned()->index()->nullable();
            $table->foreign('country_id')->references('countries')->on('id');
            $table->string('hotels_number',255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_cities');
    }
}
