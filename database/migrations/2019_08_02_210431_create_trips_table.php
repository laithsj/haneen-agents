<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trips', function (Blueprint $table) {
            $table->engine = 'MyIsam';
            $table->bigIncrements('id');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users');
            $table->enum('trip_type',['0','1'])->nullable()->default('0');
            $table->timestamp('trip_start_date')->nullable();
            $table->timestamp('trip_end_date')->nullable();
            $table->enum('trip_age',['0','1','2'])->nullable()->default('0');
            $table->integer('country_id')->unsigned()->index()->nullable();
            $table->text('extra_details')->nullable();
            $table->decimal('trip_price_per_person', 10, 2)->nullable();
            $table->decimal('trip_extra_price', 10, 2)->nullable();
            $table->decimal('trip_total_price', 20, 2)->nullable();
            $table->enum('currency',['USD','EURO','JOD'])->nullable()->default('JOD');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trips');
    }
}
